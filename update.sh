#!/bin/sh
# by LordNicky v2.7 20250127

rm -rf "/root/ToFoIn-master";
rm -rf "/root/tofoin.zip";
fetch -q -o "/root/tofoin.zip" "https://gitlab.com/LordNicky/ToFoIn/-/archive/master/ToFoIn-master.zip";
unzip "/root/tofoin.zip";
cd "/root/ToFoIn-master" || exit
/root/ToFoIn-master/install.sh;
