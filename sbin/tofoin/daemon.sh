#!/bin/sh
# by LordNicky v2.7.1 20250202
. /usr/local/etc/tofoin.conf

timer_function () {
if [ $(( "${current_time}" - "${test_time}" )) -ge "${TESTERPERIOD}" ]
then c="0";
while [ "${c}" -lt "${CNUMBER}" ]
do
	eval timeout "${TESTERLIMIT}s" "${TESTER}" '$RTABLE_'${c} 10 "${c}" &
	c=$(( "${c}" + 1 ));
done
test_time=$(date +%s);
fi
if [ $(( "${current_time}" - "${judge_time}" )) -ge "${JUDGEPERIOD}" ]
then timeout "${JUDGELIMIT}s" "${JUDGE}" & judge_time=$(date +%s);
fi
}

carpfix_function () {
if [ $(( "${current_time}" - "${carpfix_time}" )) -ge "${CARPFIXPERIOD}" ]
then timeout "${CARPFIXLIMIT}s" "${CARPFIX}" & carpfix_time=$(date +%s);
fi
}

defaultoptions_function () {
sudo route change default "${GATEWAY_0}" > /dev/null 2>&1;
if [ "${POSTSCRIPT_ENABLE}" -eq 0 ]
then "${POSTSCRIPT_0}";
fi
}

backupoptions_function () {
sudo route change default "${GATEWAY_BACKUP}" > /dev/null 2>&1;
if [ "${POSTSCRIPT_ENABLE}" -eq 0 ]
then "${POSTSCRIPT_BACKUP}";
fi
}

daemon_pid="${PID_DIR}/daemon.pid";
if [ ! -f "${daemon_pid}" ]
then current_time=$(date +%s); test_time=${current_time}; judge_time=${current_time}; carpfix_time=${current_time};
echo "${current_time}" > "${daemon_pid}";
echo $$ >> "${daemon_pid}";
	if ! route get default | grep -q "gateway";
	then sudo route add default "${GATEWAY_0}" 2>&1;
	fi
logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}DAEMON: Start successfully with pid $$";
timeout "${CARPFIXLIMIT}s" "${CARPFIX}" &
sleep "${CARPFIXLIMIT}";
	if [ -f "${TMP_DIR}/carpstate" ]
	then carpstate_tmp=$(sed -n 2p "${TMP_DIR}/carpstate");
	carpstate=${carpstate_tmp:-none};
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}DAEMON: Problem with carpstate module results. Exiting"; rm "${daemon_pid}"; exit 1;
	fi
date +%s > "${TMP_DIR}/prevstate";
echo "${carpstate}" >> "${TMP_DIR}/prevstate";
	if [ "${carpstate}" -eq 0 ]
	then defaultoptions_function; b=0;
	while [ "${b}" -lt "${CNUMBER}" ]
	do
		eval timeout "${TESTERLIMIT}s" "${TESTER}" '$RTABLE_'${b} 10 ${b} &
		b=$(( "${b}" + 1 ));
	done
	elif [ "${carpstate}" -eq 1 ]
	then backupoptions_function;
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}DAEMON: Problem with carpstate value. Exiting"; rm "${daemon_pid}"; exit 1;
	fi

while true
do
	unset prevstate carpstate current_time;
	current_time=$(date +%s);
	carpstate_tmp=$(sed -n 2p "${TMP_DIR}/carpstate");
	carpstate=${carpstate_tmp:-none};
	prevstate_tmp=$(sed -n 2p "${TMP_DIR}/prevstate");
	prevstate=${prevstate_tmp:-none};
	if [ "${carpstate}" -eq 0 ]
	then
		case "$prevstate" in
			0)
				timer_function;
				;;
			1)
				logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}DAEMON: CARP switched to MASTER.";
				defaultoptions_function;
				judge_time=$(date +%s);
				timer_function;
				;;
			*)
				logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}DAEMON: Strange prev state information1: '${prevstate}'";
				;;
		esac
	elif [ "${carpstate}" -eq "1" ]
	then
		case "${prevstate}" in
			0)
				timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" "judge" "" "" "nowait" & sleep 1;
				logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}DAEMON: CARP switched to BACKUP.";
				backupoptions_function;
				;;
			1)
				;;

			*)
				logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}DAEMON: Strange prev state information2: '${prevstate}'";
				;;
		esac
	fi
	date +%s > "${TMP_DIR}/prevstate";
	echo "${carpstate}" >> "${TMP_DIR}/prevstate";
	sleep "${SENSITIVITY}";
	carpfix_function;
done
else "${WATCHDOG}" "daemon" & exit 0;
fi
