#!/bin/sh
# by LordNicky v2.7.1 20250202
. /usr/local/etc/tofoin.conf

exit_function () {
rm "${tester_pid}";
exit "${1}";
}

tester_pid="${PID_DIR}/tester_${3}.pid";
if [ ! -f "${tester_pid}" ];
then date +%s > "${tester_pid}";
echo $$ >> "${tester_pid}";
	case "${2}" in
		0|1|2|3|4|5|6|7|8|9)
			eval setfib "${1}" ping -c "${PNUMBER}" '$PTARGET_'${2};
			exit_function 0;
			;;
		10)
			timelimit=$(( "${PNUMBER}" * 10 ));
			a=0;
			while [ "${a}" -lt "${TNUMBER}" ]
			do
				if eval timeout "${timelimit}s" setfib "${1}" ping -c "${PNUMBER}" '$PTARGET_'${a} > /dev/null 2>&1;
				then date +%s > "${TMP_DIR}/result_${3}";
				echo "0" >> "${TMP_DIR}/result_${3}";
				echo "${a}" >> "${TMP_DIR}/result_${3}";
				exit_function 0;
			fi
			a=$(( "${a}" + 1 ));
			done
			date +%s > "${TMP_DIR}/result_${3}";
			echo "1" >> "${TMP_DIR}/result_${3}";
			echo "1" >> "${TMP_DIR}/result_${3}";
			exit_function 0;
			;;
		*)
			setfib "${1}" ping -c "${PNUMBER}" "${2}";
			exit_function 1;
			;;
	esac
else timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" "tester" "${tester_pid}" "${3}" & exit 0;
fi
