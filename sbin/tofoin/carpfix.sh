#!/bin/sh
# by LordNicky v2.7.1 20250202
. /usr/local/etc/tofoin.conf

exit_function () {
rm "${carpfix_pid}";
exit "${1}";
}

carpnull_function () {
b=$(sysctl -n net.inet.carp.demotion);
while [ "$b" -ne 0 ]
do
	if [ "$b" -gt 0 ]
	then sudo sysctl "net.inet.carp.demotion=-240" > /dev/null;
	elif [ "$b" -lt 0 ]
	then sudo sysctl "net.inet.carp.demotion=240" > /dev/null;
	fi
	b=$(sysctl -n net.inet.carp.demotion);
done
}

force_state_function () {
a="0";
while [ "${a}" -lt "${OIFNUMBER}" ]
do
	eval sudo ifconfig '$OIF_'${a} vhid "${CARP_VHID}" state "${1}" > /dev/null;
	a=$(( "${a}" + 1 ));
done
}

carpfix_pid="${PID_DIR}/carpfix.pid";
if [ ! -f "${carpfix_pid}" ];
then date +%s > "${carpfix_pid}";
	echo $$ >> "${carpfix_pid}";
 	if ifconfig "${MAIN_IF}" | grep carp | grep -E -q MASTER;
	then if [ "${CARPFIX_ENABLE}" -eq 0 ]
		then if ifconfig | grep carp | grep -E -q BACKUP;
			then carpnull_function;
			force_state_function "master";
			logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}CARPFIX: All CARP interfaces forced to Master";
			fi
		fi
	date +%s > "${TMP_DIR}/carpstate";
	echo "0" >> "${TMP_DIR}/carpstate";
	exit_function 0;
	elif ifconfig "${MAIN_IF}" | grep carp | grep -E -q BACKUP;
	then if [ "${CARPFIX_ENABLE}" -eq 0 ]
		then if ifconfig | grep carp | grep -E -q MASTER;
			then force_state_function "backup";
			logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}CARPFIX: All CARP interfaces forced to Backup";
			fi
		fi
	date +%s > "${TMP_DIR}/carpstate";
	echo "1" >> "${TMP_DIR}/carpstate";
	exit_function 0;
	elif ifconfig "${MAIN_IF}" | grep carp | grep -E -q INIT;
	then if [ "${CARPFIX_ENABLE}" -eq 0 ]
		then sudo sysctl net.inet.carp.demotion=2400 > /dev/null;
		sudo ifconfig "${MAIN_IF}" vhid "${CARP_VHID}" state backup > /dev/null;
		force_state_function "backup";
		logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}CARPFIX: All CARP interfaces forced from INIT to Backup";
		fi
	date +%s > "${TMP_DIR}/carpstate";
	echo "1" >> "${TMP_DIR}/carpstate";
	exit_function 0;
	else logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}CARPFIX: Strange carp state fixing.";
	date +%s > "${TMP_DIR}/carpstate";
	echo "2" >> "${TMP_DIR}/carpstate";
	exit_function 1;
	fi
else timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" "carpfix" "${carpfix_pid}" & exit 0;
fi
