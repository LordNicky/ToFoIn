#!/bin/sh
# by LordNicky v2.7.1 20250202
. /usr/local/etc/tofoin.conf

exit_function () {
date +%s > "${TMP_DIR}/judgestate";
echo "${1}" >> "${TMP_DIR}/judgestate";
rm "${judge_pid}";
exit "${1}";
}

decision_function () {
if [ "${actualchan}" -eq "${prefchan}" ];
then
	if [ "${actualchan}" -eq 0 ];
	then exit_function 0;
	elif [ "${actualchan}" -gt 0 ];
	then if [ "${actualchan}" -lt "${CNUMBER}" ];
		then echo "0" > "${TMP_DIR}/judgemeter";
		logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}JUDGE(decision): Stay at the channel ${actualchan}"; exit_function 0;
		else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(decision): Invalid actualchan '${actualchan}'"; exit_function 1;
		fi
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(decision1): Invalid actualchan '${actualchan}'"; exit_function 1;
	fi
else
	if [ "${prefchan}" -eq 0 ];
	then if [ "${actualstate}" -eq 0 ];
		then meter=$(cat "${TMP_DIR}/judgemeter");
			if [ "${meter}" -eq "${WNUMBER}" ];
			then switch_function;
			elif [ "${meter}" -lt "${WNUMBER}" ];
			then echo $(( "${meter}" + 1 )) > "${TMP_DIR}/judgemeter";
			exit_function 0;
			else echo "0" > "${TMP_DIR}/judgemeter"; exit_function 0;
			fi
		elif [ "${actualstate}" -eq 1 ]
		then logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}JUDGE(decision): Emergency switch to ${prefchan}";
		switch_function;
		else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(decision2): Invalid actualchan '${actualchan}'"; exit_function 1;
		fi
	elif [ "${prefchan}" -gt 0 ];
	then if [ "${prefchan}" -lt "${CNUMBER}" ];
		then switch_function;
		else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(decision): Invalid prefchan '${prefchan}'" exit_function 1;
		fi
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(decision1): Invalid prefchan '${prefchan}'" exit_function 1;
	fi
fi
}

switch_function () {
echo "0" > "${TMP_DIR}/judgemeter";
if [ "${prefchan}" ]
then eval gw_set='$GATEWAY_'${prefchan};
	if [ "${gw_set}" ]
	then sudo route change default "${gw_set}" 2>&1;
	logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}JUDGE(switch): Switched to gateway ${gw_set}";

		if [ "${POSTSCRIPT_ENABLE}" -eq 0 ]
		then eval '$POSTSCRIPT_'${prefchan};
		fi
	exit_function 0;
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(switch): Invalid gateway '${gw_set}'"; exit_function 1;
	fi
else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(switch): Invalid prefchan '${prefchan}'"; exit_function 1;
fi
}

createarea_function () {
b=0;
while [ "${b}" -lt "${CNUMBER}" ]
do
	current_time=$(date +%s);
	d_tmp=$(sed -n 1p "${TMP_DIR}/result_${b}");
	d=${d_tmp:-0};
	diff=$(( "${current_time}" - "${d}" ));
	if [ "${diff}" -ge 0 ];
	then if [ "${diff}" -gt "$(( "${TESTERPERIOD}" + 120 ))" ];
		then logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}JUDGE(createarea): MAX period";
		timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" &
		exit_function 1;
		fi
	else logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}JUDGE(createarea): testmodule '${b}' in future";
	timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" &
	exit_function 1;
	fi
	eval statearea_${b}="$(sed -n 2p "${TMP_DIR}/result_${b}")";
	if [ "${actualchan}" -eq "${b}" ]
	then eval actualstate='$statearea_'${b};
	fi
	b=$(( "${b}" + 1 ));
done
}

findarea_function () {
c=0;
while [ "${c}" -lt "${CNUMBER}" ]
do
	eval statearea_work='$statearea_'${c};
	if [ "${statearea_work}" -eq 0 ];
	then prefchan="${c}"; decision_function;
	else
		if [ "${statearea_work}" -ne 1 ]
		then logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}JUDGE(findarea): Invalid channel state for c='${c}'"; exit_function 1;
		fi
	fi
	c=$(( "${c}" + 1 ));
done
}

judge_pid="${PID_DIR}/judge.pid";
if [ ! -f "${judge_pid}" ];
then date +%s > "${judge_pid}";
echo $$ >> "${judge_pid}";
a=0;
while [ "${a}" -lt "${CNUMBER}" ]
do
	eval gw_chk='$GATEWAY_'${a};
	if route -n get default | grep -E -q "${gw_chk}";
	then actualchan="${a}"; break
	fi
	a=$(( "${a}" + 1 ));
done
	if [ "${actualchan}" ];
	then createarea_function;
	findarea_function;
	logger -p "${LOG_FACILITY}notice" "${LOG_PREFIX}JUDGE: All channels down"; exit_function 1;
	else logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}JUDGE: Actualchan is empty";
	prefchan=0;
	switch_function;
	fi
else timeout "${WATCHDOGLIMIT}s" "${WATCHDOG}" "judge" & exit 0;
fi
