#!/bin/sh
# by LordNicky v2.7.1 20250202
. /usr/local/etc/tofoin.conf

exit_function () {
rm "${watchdog_pid}";
exit "${1}";
}

check_function () {
if wd_kill=$(pgrep -qf "${WATCHDOG}" && sudo pkill -SIGKILL -f "${WATCHDOG}");
then logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Killed stealth watchdogs: '${wd_kill}'!";
fi
current_time=$(date +%s);
proc_name=daemon;
daemon_function; return_val=$(( "${return_val}" + $? ));
unset diff
carpfix_time_tmp="$(sed -n 1p "${TMP_DIR}/carpstate")";
carpfix_time=${carpfix_time_tmp:-0};
diff=$(( "${current_time}" - "${carpfix_time}" ));
[ "${diff}" -ge "$(( "${CARPFIXPERIOD}" * 2 ))" ] && logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Check carpfix settings!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
[ "${diff}" -lt 0 ] && logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG: Check date 1!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
unset diff
carpstate=$(sed -n 2p "${TMP_DIR}/carpstate");
case ${carpstate} in
	0)
		a="0";
		while [ "${a}" -lt "${CNUMBER}" ]
		do
			tester_time_tmp=$(sed -n 1p "${TMP_DIR}/result_${a}");
			tester_time=${tester_time_tmp:-0};
			diff=$(( "${current_time}" - "${tester_time}" ));
			[ "${diff}" -ge "$(( "${TESTERPERIOD}" * 2 ))" ] && logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Check tester ${a} settings!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
			[ "${diff}" -lt 0 ] && logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG: Check date 2!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
			a=$(( "${a}" + 1));
		done
		unset diff
		if ! judge_time_tmp="$(sed -n 1p "${TMP_DIR}/judgestate")";
		then logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}WATCHDOG: Judgemeter not found, trying to wait.";
		sleep ${JUDGEPERIOD};
		judge_time_tmp="$(sed -n 1p "${TMP_DIR}/judgestate")";
		current_time=$(date +%s);
		fi
		judge_time=${judge_time_tmp:-0};
		diff=$(( "${current_time}" - "${judge_time}" ));
		[ "${diff}" -ge "$(( "${JUDGEPERIOD}" * 2 ))" ] && logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Check judge settings!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
		[ "${diff}" -lt 0 ] && logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG: Check date 3!" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
		return 0;
		;;
	1)
		return 0;
		;;
	*)
		set -- "${MAIN_IF}";
		a="0";
		while [ "${a}" -lt "${OIFNUMBER}" ]
		do
			eval if_list_temp='$OIF_'${a};
			set -- "$@" "${if_list_temp}"
			a=$(( "${a}" + 1 ));
		done
		for i in "$@";
		do
			sudo ifconfig "${i}" down;
			sudo ifconfig "${i}" up;
		done
		logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Unknown carp state. Interfaces '$*' was rebooted.";
		return 1;
		;;
esac
}

carpfix_function () {
proc_pid_file="${PID_DIR}/carpfix.pid"; limit="${CARPFIXLIMIT}"; proc_s_name="${proc_name}"; sh_name="${CARPFIX}";
module_function; return_mod=$?;
unset proc_pid_file limit proc_s_name sh_name return_dec;
return "${return_mod}";
}

daemon_function () {
proc_s_name="${proc_name}";
daemon_time=$(sed -n 1p "${TMP_DIR}/prevstate");
diff=$(( "${current_time}" - "${daemon_time}" ));
if [ "${diff}" -ge "$(( "${SENSITIVITY}" * 2 ))" ];
then sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null; exit_function 2;
fi
if [ "${diff}" -lt 0 ]
then logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG(DAEMON): Check date!";
sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null; exit_function 2;
fi
! pgrep -qf "${DAEMON}" && sudo /usr/local/etc/rc.d/tofoin restart >> /dev/null && exit_function 2;
return 0;
}

judge_function () {
proc_pid_file="${PID_DIR}/judge.pid"; limit="${JUDGELIMIT}"; proc_s_name="${proc_name}"; sh_name="${JUDGE}";
module_function; return_mod=$?;
unset proc_pid_file limit proc_s_name sh_name return_dec;
return "${return_mod}";
}

tester_function () {
proc_pid_file="${rec_pid}"; limit="${TESTERLIMIT}"; proc_s_name="${proc_name} ${rec_num} channel"; sh_name="${TESTER} ${rec_num}";
module_function; return_mod=$?;
unset proc_pid_file limit proc_s_name sh_name return_dec;
return "${return_mod}";
}

watchdog_function () {
proc_pid_file="${PID_DIR}/watchdog.pid"; limit="${WATCHDOGLIMIT}"; proc_name="watchdog"; proc_s_name="${proc_name}"; sh_name="${WATCHDOG}";
current_time=$(date +%s);
decision_function "check"; return_dec=$?;
return "${return_dec}";
}

module_function () {
unset return_mod;
if [ -e "${proc_pid_file}" ];
then proc_pid=$(sed -n 2p "${proc_pid_file}");
else logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG(module): PID file not found for ${proc_s_name}.";
	if proc_pid=$(pgrep -f "${sh_name}");
	then kill_function;
	return 0;
	else logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG(module): PID not found for ${proc_s_name}.";
	return 1;
	fi
fi
current_time=$(date +%s);
case ${rec_wait} in
	"check")
		decision_function "check"; return_dec=$?;
		[ "${return_dec}" -eq 10 ] && return 0;
		;;
	"nowait")
		decision_function "nowait"; return_dec=$?;
		;;
	*)
		decision_function "check"; return_dec=$?;
		;;
esac
case ${return_dec} in
	0)
		;;
	1)
		return 1;
		;;
	10)
		sleep "${limit}";
		current_time=$(date +%s);
		decision_function "afterwait";
		;;
	*)
		logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG(module): Bad return_dec '${return_dec}' for ${proc_s_name}.";
		return 1;
		;;
esac
}

decision_function () {
unset start_time_tmp start_time diff
case ${1} in
	check)
		start_time_tmp=$(sed -n 1p "${proc_pid_file}");
		start_time=${start_time_tmp:-0};
		diff="$(( "${current_time}" - "${start_time}" ))";
		if [ "${diff}" -ge 0 ];
		then if [ "${diff}" -lt "${limit}" ];
			then if [ "${proc_name}" = "watchdog" ]
				then logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}WATCHDOG(decision): Other ${proc_s_name} already working, exit.";
				exit 0;
				else logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}WATCHDOG(decision): ${proc_s_name} now working, trying to wait.";
				proc_temp_pid="${proc_pid}";
				return 10;
				fi
			fi
		else logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG(decision): Time err in ${proc_s_name} = ${diff}.";
		kill_function;
       		return 0;
		fi
		;;
	afterwait)
		if [ -e "${proc_pid_file}" ];
		then proc_pid_tmp=$(sed -n 2p "${proc_pid_file}"); proc_pid="${proc_pid_tmp:-none}"
		fi
		if [ "${proc_pid}" = "${proc_temp_pid}" ];
		then kill_function;
		return 0;
		else logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}WATCHDOG(decision): Pid of ${proc_s_name} was changed, no action.";
		return 0;
		fi
		;;
	nowait)
		kill_function;
		return 0;
		;;
	*)
		logger -p "${LOG_FACILITY}err" "${LOG_PREFIX}WATCHDOG(decision): Bad decision_function argument '${1}'.";
		return 1;
		;;
esac
}

kill_function () {
if [ "${proc_pid}" ] && [ "${proc_name}" ]
then if pgrep -f "tofoin/${proc_name}" | grep -qo "${proc_pid}";
	then logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG(kill): ${proc_name} working during ${diff}, kill him.";
	sudo kill -9 "${proc_pid}" >> /dev/null;
	fi
[ "${proc_name}" != "watchdog" ] && [ "${proc_pid_file}" ] && rm "${proc_pid_file}" && logger -p "${LOG_FACILITY}info" "${LOG_PREFIX}WATCHDOG(kill): Pid file '${proc_s_name}' removed.";
fi
}

rec_name="${1}";
rec_pid="${2}";
rec_num="${3}";
rec_wait="${4}";

return_val="0";
watchdog_pid="${PID_DIR}/watchdog.pid";
if [ ! -f "${watchdog_pid}" ];
then date +%s > "${watchdog_pid}";
echo $$ >> "${watchdog_pid}";
else proc_pid=$(sed -n 2p "${watchdog_pid}");
watchdog_function; return_val=$(( "${return_val}" + $? ));
unset proc_pid proc_pid_file limit proc_s_name sh_name return_dec proc_name;
date +%s > "${watchdog_pid}";
echo $$ >> "${watchdog_pid}";
fi
proc_name="${rec_name:-all}";
case ${proc_name} in
	all)
		check_function; return_val=$(( "${return_val}" + $? ));
		;;
	daemon)
		current_time=$(date +%s);
		daemon_function; return_val=$(( "${return_val}" + $? ));
		;;
	carpfix)
		carpfix_function; return_val=$(( "${return_val}" + $? ));
		;;
	judge)
		judge_function; return_val=$(( "${return_val}" + $? ));
		;;
	tester)
		tester_function; return_val=$(( "${return_val}" + $? ));
		;;
	*)
		logger -p "${LOG_FACILITY}warning" "${LOG_PREFIX}WATCHDOG: Incorrect process name.";
		;;
esac
exit_function "${return_val}";
