Scripts for using to switch between main and several reserve external (Internet) channels. Also can be used several routers to failover hardware by carp. More detail by [link](https://habr.com/ru/post/421857/), but only russian now. If it will neccessary, I will translate main parts to english.

From version 2.0 its fully posix sh scripts.
From version 2.7 operating principle back to switch default gateway instead of switch route table.

-----------------------------------------------------------

Installation:
- download and unpack master archive to /root/ dir
- check and change options by editing install.sh
- run install.sh
- configure and run carp
- configure fibs for all external channels
- edit /usr/local/etc/tofoin.conf
- add tofoin_enable="YES" to rc.conf for autostart at boot
- sudoers, syslog and cron examples placed usretc dir

To use minimal functionallity you need to install or/and configure:
- sudo
- kernel with n+1 routing tables(fibs), where n - number of internet channels
- carp atleast at one interface

-----------------------------------------------------------

To do:
- add working without carp
- add tofoin to portstree

Be free to create an issue if you found a bug.
