####      tofoin.conf      ####
# by LordNicky v2.7 20250127  #
# Little about the modules and about what function they perform.
# Daemon - running by rc script and run all other modules.
# Tester - testing the availability of the Internet on selected channel.
# Judge - test results analysis, the decision to switch from one channel
# to another.
# Carpfix - check carp state and maintain the same state on all interfaces.
# Watchdog - testing and resolve faults of the other scripts.
#
# Switching based on changing default route in default fib.
# Other fibs uses for ping-based testing internet channels.
#
#### Common settings: ####
#
# Number of routers.
RNUMBER="2"
#
# Number of Internet channels.
CNUMBER="2"
#
# Main Internet channel properties:
# Id number of the routing table.
RTABLE_0="1"
#
# Default gateway for main channel.
GATEWAY_0="2.2.2.2"
#
# Reserve Internet channel properties:
# Id number of the routing table.
RTABLE_1="2"
#
# Default gateway for main channel.
GATEWAY_1="4.4.4.4"
# Additional internet channels can be added by increasing 'CNUMBER'
# and adding 'RTABLE_2', 'GATEWAY_2' etc.
#
# The default location for SYSLOG notifications and warnings
# is system-dependent (typically /var/log/messages
# or /var/log/syslog).  To change this default location, LOG_FACILITY variable
# must be specified and syslog must be configured also.
# Log levels for using in syslog config: err, warning, notice, info.
# For example:
#LOG_FACILITY="local2."
#
# Log prefix to add some additional information before log messages.
# For example, to get current time or something else:
#LOG_PREFIX="$(date +%d'-'%m'-'%y' '%H':'%M':'%S) [tofoin] : "
#
# Enable sh-scripts execution after switching to any channel.
# Set to '0' to enable. '1' to disable(default).
# Paths to scripts are assigned below in 'File path' section.
POSTSCRIPT_ENABLE="1"
#
#### Carp settings: ####
#
# Internal CARP ip address.
# This address will use as gateway for internet access in backup carp state.
GATEWAY_BACKUP="3.3.3.3"
#
# To fix synchronous carp interfaces working set CARPFIX_ENABLE=0.
CARPFIX_ENABLE="1"
#
# Carp vhid.
# Script logic mean, that all describe here carp interfaces have the same vhid.
CARP_VHID="1"
#
# Main interface name. Carp state will based on the state of 'MAIN_IF'.
# If 'CARPFIX_ENABLE=0', all other interfaces will force state of 'MAIN_IF'.
MAIN_IF="eth0"
#
# All other carp interfaces and their number.
# Additional interfaces can be added by increasing 'OIFNUMBER'
# and adding 'OIF_3' etc.
OIFNUMBER="3"
OIF_0="eth0"
OIF_1="eth1"
OIF_2="eth3"
#
#### Tester settings: ####
#
# Number of ping targets(up to 10).
TNUMBER="2"
#
# URL's supposed to be used for diagnostic of the availability
# of the Internet channel. PTARGET_0 should be domain name, and
# PTARGET_1 should be IP address.
# The resources can be different.
PTARGET_0="ya.ru"
PTARGET_1="8.8.8.8"
# Additional targets can be added by increasing 'TNUMBER'
# and adding 'PTARGET_2' etc.
# Testing is performed from 0 to last until the first success.
#
# Count of icmp packets used for send to every target.
PNUMBER="2"
#
#### Judge settings: ####
#
# Amount of tests that successfully passed before returning
# to the main channel. Thereby, time elapsed since the restore
# the work main channel is approximately (WNUMBER+1)*JUDGEPERIOD
# seconds.
WNUMBER="3"
#
#### Timers settings: ####
#
# Check period timers for launch Tester, Judge and Carpfix modules.
# Usually enough 60.
SENSITIVITY="60"
#
# Launch period of the module "Tester" (in seconds).
# Strongly not recomended to set a value less than 60.
TESTERPERIOD="240"
#
# Lunch period of the module "Judge" (in seconds).
# Strongly not recomended to set a value less than TESTERPERIOD.
# Usually enough TESTERPERIOD + 60.
JUDGEPERIOD="300"
#
# Launch period of the module "Carpfix" (in seconds).
CARPFIXPERIOD="20"
#
#### Limit settings: ####
# Limit variables set timeout restrictions for modules execution.
#
# The maximum operating time for the module Tester.
TESTERLIMIT="20"
#
# The maximum operating time for the module Judge.
JUDGELIMIT="30"
#
# The maximum operating time for the module Carpfix.
CARPFIXLIMIT="30"
#
# The maximum operating time for the module Watchdog.
WATCHDOGLIMIT="150"
#
#### File paths: ####
#
# Paths for scripts executable after swithing.
# For example, firewall updating or restart any other service.
# 'POSTSCRIPT_0', 'POSTSCRIPT_1' etc uses when switching to appropriate
# internet channel.
# 'POSTSCRIPT_BACKUP' uses when switching gateway to another tofoin router.
#POSTSCRIPT_BACKUP=/usr/local/sbin/tofoin/postsctipt0.sh
#POSTSCRIPT_0=/usr/local/sbin/tofoin/postsctipt0.sh
#
# Paths for all ToFoIn files:
# It must be set, there is usually no need to change this.
DAEMON="/usr/local/sbin/tofoin/daemon.sh"
TESTER="/usr/local/sbin/tofoin/tester.sh"
JUDGE="/usr/local/sbin/tofoin/judge.sh"
CARPFIX="/usr/local/sbin/tofoin/carpfix.sh"
WATCHDOG="/usr/local/sbin/tofoin/watchdog.sh"
#
# Directory for temp files of all modules. Will create at start.
# Default: '/tmp/tofoin'.
TMP_DIR="/tmp/tofoin"
#
# Directory for PID files of all modules.
# Default: '/var/run/tofoin'.
PID_DIR="/var/run/tofoin"
