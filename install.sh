#!/bin/sh
# by LordNicky v2.7.1 20250202

if ! pw usershow tofoin > /dev/null;
then printf "Creating tofoin user and group...\n";
pw groupadd -n tofoin -g 1500;
pw useradd -n tofoin -u 1500 -G tofoin -d /nonexistent -s /bin/sh -c "Toogle Failover of Internet";
fi

printf "Copying ToFoIn files...\n";
[ -e "/usr/local/sbin/tofoin" ] && rm -r "/usr/local/sbin/tofoin";
mkdir "/usr/local/sbin/tofoin";
cp ./sbin/tofoin/* "/usr/local/sbin/tofoin/";

if [ -e "/usr/local/etc/tofoin.conf" ];
then cp "./etc/tofoin.conf" "/usr/local/etc/tofoin.conf.default";
else cp "./etc/tofoin.conf" "/usr/local/etc/";
fi

cp "./etc/rc.d/tofoin" "/usr/local/etc/rc.d/";

printf "Install/update any thirdparty configs for ToFoIn? (y/n)\n";
IFS= read -r main_reply;
if [ "${main_reply}" = "y" ]
then set -- "sudoers.d" "syslog.d" "newsyslog.conf.d";
	for i in "${@}";
	do
	printf "Install/update %s config for ToFoIn? (y/n)\n" "${i}";
	IFS= read -r si_reply;
	case "${si_reply}" in
		y|Y)
			cp ./etc/${i}/* "/usr/local/etc/${i}/";
			printf "Done.\n";
			;;
		n|N)
			printf "Skipping.\n";
			;;
		*)
			;;
	esac				
	done

	printf "Install/update cron.d config for ToFoIn? (y/n)\n";
	IFS= read -r cron_reply;
	case "${cron_reply}" in
		y|Y)
			cp "./etc/cron.d/tofoin" "/etc/cron.d/";
			printf "Done.\n";
			;;
		n|N)
			printf "Skipping.\n";
			;;
		*)
			;;
	esac			
else printf "Skipping.\n";
fi

printf "Installation done!\n";	
